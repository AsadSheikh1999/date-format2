import Foundation

public func code(for title: String,
                    sampleCode: () -> Void) {
print("""
_____________________________
Results for: '\(title)'
""")
  sampleCode()
}


public enum DateFormatterStyleEnums {
    public static let styles:[DateFormatter.Style] = [.none, .short, .medium, .long, .full]
}
public extension DateFormatter.Style {
    var description: String {
        switch self {
        case .none:
           return  ".none"
        case .short:
            return ".short"
        case .medium:
            return ".medium"
        case .long:
            return ".long"
        case .full:
            return ".full"
        @unknown default:
            return "unknown"
        }
    }
}

let now = Date()
code(for: "DateFormatter DateStyles and TimeStyles") {
    print(now)
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .full
    dateFormatter.timeStyle = .full
    print(dateFormatter.string(from: now))
}

code(for: "DateFormatter DateStyles + Locale") {
    let dateFormatter = DateFormatter()
//    dateFormatter.locale = Locale(identifier: "en-GB")
//    print(Locale.availableIdentifiers)
//    dateFormatter.dateStyle = .full
//    print(dateFormatter.string(from: now))
    for dateStyle in DateFormatterStyleEnums.styles {
        dateFormatter.dateStyle = dateStyle
        print("\(dateStyle.description) - \(dateFormatter.string(from: now))")
    }
}

code(for: "DateFormatter TimeStyles + Locale") {
    let dateFormatter = DateFormatter()
//    dateFormatter.locale = Locale(identifier: "de")
    for timeStyle in DateFormatterStyleEnums.styles {
        dateFormatter.timeStyle = timeStyle
        print("\(timeStyle.description) - \(dateFormatter.string(from: now))")
    }
}

code(for: "Time Zones") {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .long
    dateFormatter.timeStyle = .long
    dateFormatter.timeZone = TimeZone(abbreviation: "NZST")
//    print(TimeZone.knownTimeZoneIdentifiers)
//    print(TimeZone.abbreviationDictionary)
    print(dateFormatter.string(from: now))
}

code(for: "DateFormatter DateFormat") {
    // https://www.datetimeformatter.com/how-to-format-date-time-in-swift/
    // https://nsdateformatter.com
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEEE, MMMM d, yyyy - hh:mm:ss a zzz"
    print(dateFormatter.string(from: now))

}
