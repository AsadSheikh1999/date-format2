//
//  dateFormatProgrammingApp.swift
//  dateFormatProgramming
//
//  Created by mohammad.sheikh on 30/03/22.
//

import SwiftUI

@main
struct dateFormatProgrammingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
